import React, { Component } from 'react';
import { Link } from 'react-router-dom';

export class Catalog extends Component {
  constructor(props){
    super(props)
    this.state = {
      todos: [
        {
            id: 1,
            task: 'Level up for Antony',
            isCompleted: false
        },
        {
            id: 2,
            task: 'Follow up of team zilla',
            isCompleted: true
        },
        {
            id: 3,
            task: 'Project meeting with CEO',
            isCompleted: false
        },
        {
            id: 4,
            task: 'Duplicate a project for new customer',
            isCompleted: true
        }
      ],
      inputValue: '',
    }
    this.statusChangedHandler = this.statusChangedHandler.bind(this);
    this.addTodo = this.addTodo.bind(this);
    this.removeTodo = this.removeTodo.bind(this);
    this.inputChangeHandler = this.inputChangeHandler.bind(this);
  }
  statusChangedHandler(event, id) {

    //const todoIndex = this.state.todos.findIndex( t => t.id === id );
    const todo = {...this.state.todos[id]};
    todo.isCompleted = event.target.checked;

    const todos = [...this.state.todos];
    todos[id] = todo;

    this.setState({
        todos: todos
    })
  }

  addTodo (event) {
      event.preventDefault();

      const todos = [...this.state.todos];
      todos.unshift({
          id: todos.length ? todos[todos.length - 1].id + 1 : 1,
          task: this.state.inputValue,
          isCompleted: false

      })

      this.setState({
          todos: todos,
          inputValue: ''
      })
  }

  removeTodo (index) {
      const todos = [...this.state.todos];
      todos.splice(index, 1);

      this.setState({
          todos: todos
      })
  }

  inputChangeHandler(event) {
      this.setState({
          inputValue: event.target.value
      });
  }

  render() {
    return (
      <div>
        <div className="row">
          <div className="col-md-4 grid-margin stretch-card">
            <div className="card">
              <div className="card-body">
                <div className="d-sm-flex flex-row flex-wrap text-center text-sm-left align-items-center">
                  <i className="ti-user icon-md text-muted mb-0 mb-md-3 mb-xl-0"></i>
                  <div className="ml-xl-3 mt-2 mt-xl-0">
                    <h6>Application Access</h6>
                    <p className="text-muted">Requests for Distributed Applications</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="col-md-4 grid-margin stretch-card">
            <div className="card">
              <div className="card-body">
                <div className="d-sm-flex flex-row flex-wrap text-center text-sm-left align-items-center">
                <i className="ti-server icon-md text-muted mb-0 mb-md-3 mb-xl-0"></i>
                  <div className="ml-xl-3 mt-2 mt-xl-0">
                    <h6>Mainframe Access</h6>
                    <p className="text-muted">Requests for Mainframe Applications</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="col-md-4 grid-margin stretch-card">
            <div className="card">
              <div className="card-body">
                <div className="d-sm-flex flex-row flex-wrap text-center text-sm-left align-items-center">
                <i className="ti-headphone-alt icon-md text-muted mb-0 mb-md-3 mb-xl-0"></i>
                  <div className="ml-xl-3 mt-2 mt-xl-0">
                    <h6>Generic Access</h6>
                    <p className="text-muted">All Other Capabilities</p>
                  </div>
                </div>
              </div>
            </div>
          </div>

        </div>

        <div className="row">

        <div className="col-md-4 grid-margin stretch-card">
          <div className="card">
            <div className="card-body">
              <h4 className="card-title">Services</h4>

              <div className="d-flex align-items-center pb-3 border-bottom">
                <i className="ti-package icon-md text-muted mb-0 mb-md-3 mb-xl-0"></i>
                <div className="ml-3">
                  <h6 className="mb-1">2-Factor Requests</h6>
                </div>
                <Link className="btn ml-auto px-1 py-1 text-success" to="/intake-forms/mfa">Request</Link>
              </div>
              <div className="d-flex align-items-center pb-3 border-bottom">
                <i className="ti-package icon-md text-muted mb-0 mb-md-3 mb-xl-0"></i>
                <div className="ml-3">
                  <h6 className="mb-1">Identity Access Management (IAM)</h6>
                </div>
                <Link className="btn ml-auto px-1 py-1 text-success" to="/intake-forms/iam">Request</Link>
              </div>
              <div className="d-flex align-items-center pb-3 border-bottom">
                <i className="ti-package icon-md text-muted mb-0 mb-md-3 mb-xl-0"></i>
                <div className="ml-3">
                  <h6 className="mb-1">Distributed Systems</h6>
                </div>
                <Link className="btn ml-auto px-1 py-1 text-success" to="/intake-forms/distributed-systems">Request</Link>
              </div>
              <div className="d-flex align-items-center pb-3 border-bottom">
                <i className="ti-package icon-md text-muted mb-0 mb-md-3 mb-xl-0"></i>
                <div className="ml-3">
                  <h6 className="mb-1">Physical Systems</h6>
                </div>
                <Link className="btn ml-auto px-1 py-1 text-success" to="/intake-forms/physical-systems">Request</Link>
              </div>
              <div className="d-flex align-items-center pb-3 border-bottom">
                <i className="ti-package icon-md text-muted mb-0 mb-md-3 mb-xl-0"></i>
                <div className="ml-3">
                  <h6 className="mb-1">Access Review System</h6>
                </div>
                <Link className="btn ml-auto px-1 py-1 text-success" to="/intake-forms/ars">Request</Link>
              </div>
              <div className="d-flex align-items-center pb-3 border-bottom">
                <i className="ti-package icon-md text-muted mb-0 mb-md-3 mb-xl-0"></i>
                <div className="ml-3">
                  <h6 className="mb-1">Active Directory Infrastructure</h6>
                </div>
                <Link className="btn ml-auto px-1 py-1 text-success" to="/intake-forms/active-directory">Request</Link>
              </div>
              <div className="d-flex align-items-center pb-3 border-bottom">
                <i className="ti-package icon-md text-muted mb-0 mb-md-3 mb-xl-0"></i>
                <div className="ml-3">
                  <h6 className="mb-1">CyberArk Enterprise Password Vault</h6>
                </div>
                <Link className="btn ml-auto px-1 py-1 text-success" to="/intake-forms/cyberark-pv">Request</Link>
              </div>
              <div className="d-flex align-items-center pb-3 border-bottom">
                <i className="ti-package icon-md text-muted mb-0 mb-md-3 mb-xl-0"></i>
                <div className="ml-3">
                  <h6 className="mb-1">CyberArk Privileged Session Manager</h6>
                </div>
                <Link className="btn ml-auto px-1 py-1 text-success" to="/intake-forms/cyberark-psm">Request</Link>
              </div>
              <div className="d-flex align-items-center pb-3 border-bottom">
                <i className="ti-package icon-md text-muted mb-0 mb-md-3 mb-xl-0"></i>
                <div className="ml-3">
                  <h6 className="mb-1">Enclave Requests</h6>
                </div>
                <Link className="btn ml-auto px-1 py-1 text-success" to="/intake-forms/enclave">Request</Link>
              </div>
              <div className="d-flex align-items-center pb-3 border-bottom">
                <i className="ti-package icon-md text-muted mb-0 mb-md-3 mb-xl-0"></i>
                <div className="ml-3">
                  <h6 className="mb-1">IAM Application and User Access Certification Questions</h6>
                </div>
                <Link className="btn ml-auto px-1 py-1 text-success" to="/intake-forms/iam-access-questions">Request</Link>
              </div>
              <div className="d-flex align-items-center pb-3 border-bottom">
                <i className="ti-package icon-md text-muted mb-0 mb-md-3 mb-xl-0"></i>
                <div className="ml-3">
                  <h6 className="mb-1">IAM Application Onboarding</h6>
                </div>
                <Link className="btn ml-auto px-1 py-1 text-success" to="/intake-forms/iam-app-onboarding">Request</Link>
              </div>
              <div className="d-flex align-items-center pb-3 border-bottom">
                <i className="ti-package icon-md text-muted mb-0 mb-md-3 mb-xl-0"></i>
                <div className="ml-3">
                  <h6 className="mb-1">IAM Role Lifecycle Management</h6>
                </div>
                <Link className="btn ml-auto px-1 py-1 text-success" to="/intake-forms/iam-role-lifecycle">Request</Link>
              </div>
            </div>
          </div>
        </div>

        <div className="col-md-4 grid-margin stretch-card">
          <div className="card">
            <div className="card-body">
              <h4 className="card-title">Services</h4>

              <div className="d-flex align-items-center pb-3 border-bottom">
                <i className="ti-folder icon-md text-muted mb-0 mb-md-3 mb-xl-0"></i>
                <div className="ml-3">
                  <h6 className="mb-1">CICS Change Transaction Request</h6>
                </div>
                <Link className="btn ml-auto px-1 py-1 text-success" to="/intake-forms/cics-change-transaction">Request</Link>
              </div>
              <div className="d-flex align-items-center pb-3 border-bottom">
                <i className="ti-folder icon-md text-muted mb-0 mb-md-3 mb-xl-0"></i>
                <div className="ml-3">
                  <h6 className="mb-1">CICS File Request</h6>
                </div>
                <Link className="btn ml-auto px-1 py-1 text-success" to="/intake-forms/cics-file">Request</Link>
              </div>
              <div className="d-flex align-items-center pb-3 border-bottom">
                <i className="ti-folder icon-md text-muted mb-0 mb-md-3 mb-xl-0"></i>
                <div className="ml-3">
                  <h6 className="mb-1">CICS Generic Request</h6>
                </div>
                <Link className="btn ml-auto px-1 py-1 text-success" to="/intake-forms/cics-generic">Request</Link>
              </div>
              <div className="d-flex align-items-center pb-3 border-bottom">
                <i className="ti-folder icon-md text-muted mb-0 mb-md-3 mb-xl-0"></i>
                <div className="ml-3">
                  <h6 className="mb-1">CICS Transaction Request</h6>
                </div>
                <Link className="btn ml-auto px-1 py-1 text-success" to="/intake-forms/cics-transaction">Request</Link>
              </div>

              <div className="d-flex align-items-center pb-3 border-bottom">
                <i className="ti-folder icon-md text-muted mb-0 mb-md-3 mb-xl-0"></i>
                <div className="ml-3">
                  <h6 className="mb-1">Mainframe Data Access Request</h6>
                </div>
                <Link className="btn ml-auto px-1 py-1 text-success" to="/intake-forms/mainframe-data-access">Request</Link>
              </div>
              <div className="d-flex align-items-center pb-3 border-bottom">
                <i className="ti-folder icon-md text-muted mb-0 mb-md-3 mb-xl-0"></i>
                <div className="ml-3">
                  <h6 className="mb-1">Mainframe General RACF Request</h6>
                </div>
                <Link className="btn ml-auto px-1 py-1 text-success" to="/intake-forms/mainframe-racf-generic">Request</Link>
              </div>
              <div className="d-flex align-items-center pb-3 border-bottom">
                <i className="ti-folder icon-md text-muted mb-0 mb-md-3 mb-xl-0"></i>
                <div className="ml-3">
                  <h6 className="mb-1">Mainframe Account Generic Request</h6>
                </div>
                <Link className="btn ml-auto px-1 py-1 text-success" to="/intake-forms/mainframe-account-generic">Request</Link>
              </div>
              <div className="d-flex align-items-center pb-3 border-bottom">
                <i className="ti-folder icon-md text-muted mb-0 mb-md-3 mb-xl-0"></i>
                <div className="ml-3">
                  <h6 className="mb-1">Mainframe Printer Request</h6>
                </div>
                <Link className="btn ml-auto px-1 py-1 text-success" to="/intake-forms/mainframe-printer">Request</Link>
              </div>
              <div className="d-flex align-items-center pb-3 border-bottom">
                <i className="ti-folder icon-md text-muted mb-0 mb-md-3 mb-xl-0"></i>
                <div className="ml-3">
                  <h6 className="mb-1">Mainframe - COE (MFSR)</h6>
                </div>
                <Link className="btn ml-auto px-1 py-1 text-success" to="/intake-forms/mainframe-coe">Request</Link>
              </div>

              <div className="d-flex align-items-center pb-3 border-bottom">
                <i className="ti-folder icon-md text-muted mb-0 mb-md-3 mb-xl-0"></i>
                <div className="ml-3">
                  <h6 className="mb-1">Access to Mainframe Services and Resources</h6>
                </div>
                <Link className="btn ml-auto px-1 py-1 text-success" to="/intake-forms/mainframe-services">Request</Link>
              </div>
              <div className="d-flex align-items-center pb-3 border-bottom">
                <i className="ti-folder icon-md text-muted mb-0 mb-md-3 mb-xl-0"></i>
                <div className="ml-3">
                  <h6 className="mb-1">Mainframe Data Security Protection</h6>
                </div>
                <Link className="btn ml-auto px-1 py-1 text-success" to="/intake-forms/mainframe-data-security">Request</Link>
              </div>


            </div>
          </div>
        </div>

        <div className="col-md-4 grid-margin stretch-card">
          <div className="card">
            <div className="card-body">
              <h4 className="card-title">Services</h4>
              <div className="d-flex align-items-center pb-3 border-bottom">
                <i className="ti-agenda icon-md text-muted mb-0 mb-md-3 mb-xl-0"></i>
                <div className="ml-3">
                  <h6 className="mb-1">Non-Human Account Requests</h6>
                </div>
                <Link className="btn ml-auto px-1 py-1 text-success" to="/intake-forms/non-human-account">Request</Link>
              </div>
              <div className="d-flex align-items-center pb-3 border-bottom">
                <i className="ti-agenda icon-md text-muted mb-0 mb-md-3 mb-xl-0"></i>
                <div className="ml-3">
                  <h6 className="mb-1">Service ID Accounts</h6>
                </div>
                <Link className="btn ml-auto px-1 py-1 text-success" to="/intake-forms/service-id-account">Request</Link>
              </div>
              <div className="d-flex align-items-center pb-3 border-bottom">
                <i className="ti-agenda icon-md text-muted mb-0 mb-md-3 mb-xl-0"></i>
                <div className="ml-3">
                  <h6 className="mb-1">TCP/IP/VTAM Generic Resource</h6>
                </div>
                <Link className="btn ml-auto px-1 py-1 text-success" to="/intake-forms/vram-generic">Request</Link>
              </div>
              <div className="d-flex align-items-center pb-3 border-bottom">
                <i className="ti-agenda icon-md text-muted mb-0 mb-md-3 mb-xl-0"></i>
                <div className="ml-3">
                  <h6 className="mb-1">Manage Accounts and Access - All Other Requests</h6>
                </div>
                <Link className="btn ml-auto px-1 py-1 text-success" to="/intake-forms/generic-request">Request</Link>
              </div>
            </div>
          </div>
        </div>

        </div>

      </div>
    )
  }
}
const ListItem = (props) => {

  return (
      <li className={(props.isCompleted ? 'completed' : null)}>
          <div className="form-check">
              <label htmlFor="" className="form-check-label">
                  <input className="checkbox" type="checkbox"
                      checked={props.isCompleted}
                      onChange={props.changed}
                      /> {props.children} <i className="input-helper"></i>
              </label>
          </div>
          <i className="remove ti-close" onClick={props.remove}></i>
      </li>
  )
};

export default Catalog
