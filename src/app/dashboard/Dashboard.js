import React, { Component } from 'react';
import {Line, Bar, Doughnut, Chart} from 'react-chartjs-2';
import { ProgressBar } from 'react-bootstrap';
import { Dropdown } from 'react-bootstrap';
import Slider from "react-slick";
import { Link } from 'react-router-dom';


//Doughchart Legend
var originalDoughnutDraw = Chart.controllers.doughnut.prototype.draw;
Chart.helpers.extend(Chart.controllers.doughnut.prototype, {
  draw: function() {
    originalDoughnutDraw.apply(this, arguments);

    var chart = this.chart.chart;
    var ctx = chart.ctx;
    var width = chart.width;
    var height = chart.height;

    var fontSize = 3.125;
    ctx.font = "600 " + fontSize + "em sans-serif";
    ctx.textBaseline = "middle";
    ctx.fillStyle = "#9c9fa6";

    var text = chart.config.data.text,
        textX = Math.round((width - ctx.measureText(text).width) / 2),
        textY = height / 2;

    ctx.fillText(text, textX, textY);
  }
});


// Carousel
var performanceOverview = {
  dots: false,
  infinite: true,
  speed: 500,
  slidesToShow: 1,
  slidesToScroll: 1,
  autoplay: true,
  autoplaySpeed: 5000
};


var detailedReports = {
  dots: false,
  infinite: true,
  speed: 500,
  slidesToShow: 1,
  slidesToScroll: 1,
  autoplay: true,
  autoplaySpeed: 5000
};

export class Dashboard extends Component {
  constructor(props){
    super(props)
    this.state = {
      todos: [
        {
            id: 1,
            task: 'Level up for Antony',
            isCompleted: false
        },
        {
            id: 2,
            task: 'Follow up of team zilla',
            isCompleted: true
        },
        {
            id: 3,
            task: 'Project meeting with CEO',
            isCompleted: false
        },
        {
            id: 4,
            task: 'Duplicate a project for new customer',
            isCompleted: true
        },
        {
            id: 5,
            task: 'Meeting with Urban Team',
            isCompleted: false
        }
      ],
      inputValue: '',
    }
    this.statusChangedHandler = this.statusChangedHandler.bind(this);
    this.addTodo = this.addTodo.bind(this);
    this.removeTodo = this.removeTodo.bind(this);
    this.inputChangeHandler = this.inputChangeHandler.bind(this);
  }
  statusChangedHandler(event, id) {

    //const todoIndex = this.state.todos.findIndex( t => t.id === id );
    const todo = {...this.state.todos[id]};
    todo.isCompleted = event.target.checked;

    const todos = [...this.state.todos];
    todos[id] = todo;

    this.setState({
        todos: todos
    })
  }

  addTodo (event) {
      event.preventDefault();

      const todos = [...this.state.todos];
      todos.unshift({
          id: todos.length ? todos[todos.length - 1].id + 1 : 1,
          task: this.state.inputValue,
          isCompleted: false

      })

      this.setState({
          todos: todos,
          inputValue: ''
      })
  }

  removeTodo (index) {
      const todos = [...this.state.todos];
      todos.splice(index, 1);

      this.setState({
          todos: todos
      })
  }

  inputChangeHandler(event) {
      this.setState({
          inputValue: event.target.value
      });
  }



  SalesReportData = {
    labels: ["Jan", "Feb", "Mar", "Apr", "May"],
    datasets: [{
        label: 'Offline Sales',
        data: [480, 230, 470, 210, 330],
        backgroundColor: '#ffc100'
      },
      {
        label: 'Online Sales',
        data: [400, 340, 550, 480, 170],
        backgroundColor: '#f5a623'
      }
    ]
  };
  SalesReportOptions = {
    responsive: true,
    maintainAspectRatio: true,
    layout: {
      padding: {
        left: 0,
        right: 0,
        top: 20,
        bottom: 0
      }
    },
    scales: {
      yAxes: [{
        display: true,
        gridLines: {
          display: false,
          drawBorder: false
        },
        ticks: {
          display: false,
          min: 0,
          max: 500
        }
      }],
      xAxes: [{
        stacked: false,
        ticks: {
          beginAtZero: true,
          fontColor: "#9fa0a2"
        },
        gridLines: {
          color: "rgba(0, 0, 0, 0)",
          display: false
        },
        barPercentage: 1
      }]
    },
    legend: {
      display: false
    },
    elements: {
      point: {
        radius: 0
      }
    }
  };
  ordersChartData = {
    labels: ["10","","","20","","","30","","","40","","", "50","","", "60","","","70"],
    datasets: [
      {
        data: [200, 480, 700, 600, 620, 350, 380, 350, 850, "600", "650", "350", "590", "350", "620", "500", "990", "780", "650"],
        borderColor: [
          '#248afd'
        ],
        borderWidth: 3,
        fill: false,
        label: "Orders"
      },
      {
        data: [400, 450, 410, 500, 480, 600, 450, 550, 460, "560", "450", "700", "450", "640", "550", "650", "400", "850", "800"],
        borderColor: [
          '#ff4747'
        ],
        borderWidth: 3,
        fill: false,
        label: "Downloads"
      }
    ]
  };
  ordersChartOptions = {
    responsive: true,
    maintainAspectRatio: true,
    plugins: {
      filler: {
        propagate: false
      }
    },
    scales: {
      xAxes: [{
        display: true,
        ticks: {
          display: true,
          padding: 10
        },
        gridLines: {
          display: false,
          drawBorder: false,
          color: 'transparent',
          zeroLineColor: '#eeeeee'
        }
      }],
      yAxes: [{
        display: true,
        ticks: {
          display: true,
          autoSkip: false,
          maxRotation: 0,
          stepSize: 200,
          min: 200,
          max: 1200,
          padding: 18
        },
        gridLines: {
          display: false,
          drawBorder: false
        }
      }]
    },
    legend: {
      display: false
    },
    tooltips: {
      enabled: true
    },
    elements: {
      line: {
        tension: .35
      },
      point: {
        radius: 0
      }
    }
  };
  northAmericaData = {
    datasets: [{
      data: [100, 50, 50],
      backgroundColor: [
        "#71c016", "#ffc100", "#248afd",
      ],
      borderColor: "rgba(0,0,0,0)"
    }],

    // These labels appear in the legend and in the tooltips when hovering different arcs
    labels: [
      'Pink',
      'Blue',
      'Yellow',
    ],
    text: '90',
  };
  northAmericaOptions = {
    responsive: true,
    maintainAspectRatio: true,
    segmentShowStroke: false,
    cutoutPercentage: 78,
    elements: {
      arc: {
          borderWidth: 4
      }
    },
    legend: {
      display: false
    },
    tooltips: {
      enabled: true
    },
  };
  southAmericaData = {
    datasets: [{
      data: [60, 70, 70],
      backgroundColor: [
        "#ffc100", "#248afd", "#71c016"
      ],
      borderColor: "rgba(0,0,0,0)"
    }],

    // These labels appear in the legend and in the tooltips when hovering different arcs
    labels: [
      'Pink',
      'Blue',
      'Yellow',
    ],
    text: '76',
  };
  southAmericaOptions = {
    responsive: true,
    maintainAspectRatio: true,
    segmentShowStroke: false,
    cutoutPercentage: 78,
    elements: {
      arc: {
          borderWidth: 4
      }
    },
    legend: {
      display: false
    },
    tooltips: {
      enabled: true
    },
  };
  render () {
    return (
      <div>
        <div className="row">
          <div className="col-12 col-xl-5 mb-4 mb-xl-0 grid-margin">
            <h4 className="font-weight-bold">Hi, Priya!</h4>
            <h4 className="font-weight-normal mb-0">Welcome to Truist IA3 Dashboard,</h4>
          </div>
          <div className="col-12 col-xl-7 grid-margin">
            <div className="d-flex align-items-center justify-content-between flex-wrap">
              <div className="border-right pr-4 mb-3 mb-xl-0">
                <p className="text-muted">Open Requests</p>
                <h4 className="mb-0 font-weight-bold">15</h4>
              </div>
              <div className="border-right pr-4 mb-3 mb-xl-0">
                <p className="text-muted">In Progress</p>
                <h4 className="mb-0 font-weight-bold">10</h4>
              </div>
              <div className="border-right pr-4 mb-3 mb-xl-0">
                <p className="text-muted">Completed</p>
                <h4 className="mb-0 font-weight-bold">100</h4>
              </div>
              <div className="pr-3 mb-3 mb-xl-0">
                <p className="text-muted">Total Requests</p>
                <h4 className="mb-0 font-weight-bold">125</h4>
              </div>
              <div className="mb-3 mb-xl-0">
                <Link className="btn btn-warning rounded-0 text-white" to="/catalog/mfa_request">Create Request</Link>
              </div>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-md-3 grid-margin stretch-card">
            <div className="card">
              <div className="card-body">
                <p className="card-title text-md-center text-xl-left">Commercial Banking</p>
                <div className="d-flex flex-wrap justify-content-between justify-content-md-center justify-content-xl-between align-items-center">
                  <h3 className="mb-0 mb-md-2 mb-xl-0 order-md-1 order-xl-0">120 Applications</h3>
                  <i className="ti-calendar icon-md text-muted mb-0 mb-md-3 mb-xl-0"></i>
                </div>
                <p className="mb-0 mt-2 text-warning">Critical Apps:100 <span className="text-body ml-1"><small>D-CIO Bryce Elliot</small></span></p>
              </div>
            </div>
          </div>
          <div className="col-md-3 grid-margin stretch-card">
            <div className="card">
              <div className="card-body">
                <p className="card-title text-md-center text-xl-left">Retail Banking</p>
                <div className="d-flex flex-wrap justify-content-between justify-content-md-center justify-content-xl-between align-items-center">
                  <h3 className="mb-0 mb-md-2 mb-xl-0 order-md-1 order-xl-0">150 Applications</h3>
                  <i className="ti-user icon-md text-muted mb-0 mb-md-3 mb-xl-0"></i>
                </div>
                <p className="mb-0 mt-2 text-danger">Critical Apps:120 <span className="text-body ml-1"><small>D-CIO Matt Spriggs</small></span></p>
              </div>
            </div>
          </div>
          <div className="col-md-3 grid-margin stretch-card">
            <div className="card">
              <div className="card-body">
                <p className="card-title text-md-center text-xl-left">Digital Services</p>
                <div className="d-flex flex-wrap justify-content-between justify-content-md-center justify-content-xl-between align-items-center">
                  <h3 className="mb-0 mb-md-2 mb-xl-0 order-md-1 order-xl-0">200 Services Apps</h3>
                  <i className="ti-agenda icon-md text-muted mb-0 mb-md-3 mb-xl-0"></i>
                </div>
                <p className="mb-0 mt-2 text-success">Critical Apps:150 <span className="text-body ml-1"><small>D-CIO Ken Meyer</small></span></p>
              </div>
            </div>
          </div>
          <div className="col-md-3 grid-margin stretch-card">
            <div className="card">
              <div className="card-body">
                <p className="card-title text-md-center text-xl-left">Corporate Functions</p>
                <div className="d-flex flex-wrap justify-content-between justify-content-md-center justify-content-xl-between align-items-center">
                  <h3 className="mb-0 mb-md-2 mb-xl-0 order-md-1 order-xl-0">120 Apps</h3>
                  <i className="ti-bag icon-md text-muted mb-0 mb-md-3 mb-xl-0"></i>
                </div>
                <p className="mb-0 mt-2 text-success">Critical Apps: 70 <span className="text-body ml-1"><small>D-CIO Will Shupe, Tracy Daniels</small></span></p>
              </div>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-md-3 grid-margin stretch-card">
            <div className="card">
              <div className="card-body">
                <p className="card-title text-md-center text-xl-left">Insurance</p>
                <div className="d-flex flex-wrap justify-content-between justify-content-md-center justify-content-xl-between align-items-center">
                  <h3 className="mb-0 mb-md-2 mb-xl-0 order-md-1 order-xl-0">50 Apps</h3>
                  <i className="ti-calendar icon-md text-muted mb-0 mb-md-3 mb-xl-0"></i>
                </div>
                <p className="mb-0 mt-2 text-warning">Critical Apps:30 <span className="text-body ml-1"><small>D-CIO Will Shupe</small></span></p>
              </div>
            </div>
          </div>
          <div className="col-md-3 grid-margin stretch-card">
            <div className="card">
              <div className="card-body">
                <p className="card-title text-md-center text-xl-left">Cyber</p>
                <div className="d-flex flex-wrap justify-content-between justify-content-md-center justify-content-xl-between align-items-center">
                  <h3 className="mb-0 mb-md-2 mb-xl-0 order-md-1 order-xl-0">80 Apps</h3>
                  <i className="ti-user icon-md text-muted mb-0 mb-md-3 mb-xl-0"></i>
                </div>
                <p className="mb-0 mt-2 text-warning">Critical Apps: 75 <span className="text-body ml-1"><small>D-CIO Steve Scott</small></span></p>
              </div>
            </div>
          </div>
          <div className="col-md-3 grid-margin stretch-card">
            <Link to="/tables/basic-table">
            <div className="card">
              <div className="card-body">
                <p className="card-title text-md-center text-xl-left">Risk</p>
                <div className="d-flex flex-wrap justify-content-between justify-content-md-center justify-content-xl-between align-items-center">
                  <h3 className="mb-0 mb-md-2 mb-xl-0 order-md-1 order-xl-0">80 Apps</h3>
                  <i className="ti-user icon-md text-muted mb-0 mb-md-3 mb-xl-0"></i>
                </div>
                <p className="mb-0 mt-2 text-warning">Critical Apps: 75 <span className="text-body ml-1"><small>D-CIO Steve Scott</small></span></p>
              </div>
            </div>
            </Link>
          </div>
        </div>

        <div className="row">
          <div className="col-lg-12 grid-margin stretch-card">
            <div className="card bg-primary border-0 position-relative performance-overview-carousel">
              <div className="card-body">
                <p className="card-title text-white">IA3 State of the Union</p>
                <Slider {...performanceOverview}>
                  <div className="carousel-item">
                    <div className="row">
                      <div className="col-md-4 item">
                        <div className="d-flex flex-column flex-xl-row mt-4 mt-md-0">
                          <div className="icon icon-a text-white mr-3">
                            <i className="ti-cup icon-lg ml-3"></i>
                          </div>
                          <div className="content text-white">
                            <div className="d-flex flex-wrap align-items-center mb-2 mt-3 mt-xl-1">
                              <h3 className="font-weight-light mr-2 mb-1">Human Accounts addressed</h3>
                              <h3 className="mb-0">193</h3>
                            </div>
                            <div className="col-8 col-md-7 d-flex border-bottom border-info align-items-center justify-content-between px-0 pb-2 mb-3">
                              <h5 className="mb-0">+2877 Shared/Service accounts addressed</h5>
                              <div className="d-flex align-items-center">

                                <h5 className="mb-0"></h5>
                              </div>
                            </div>
                            <p className="text-white font-weight-light pr-lg-2 pr-xl-5"> Exceeded 2Q20 target of 287. Shared and Service accounts remediated as per hBB&T MRBA risk remediation plan </p>
                          </div>
                        </div>
                      </div>
                      <div className="col-md-4 item">
                        <div className="d-flex flex-column flex-xl-row mt-5 mt-md-0">
                          <div className="icon icon-b text-white mr-3">
                            <i className="ti-bar-chart icon-lg ml-3"></i>
                          </div>
                          <div className="content text-white">
                            <div className="d-flex flex-wrap align-items-center mb-2 mt-3 mt-xl-1">
                              <h3 className="font-weight-light mr-2 mb-1">SOX Updates</h3>
                              <h3 className="mb-0">Critical Findings</h3>
                            </div>
                            <div className="col-8 col-md-7 d-flex border-bottom border-info align-items-center justify-content-between px-0 pb-2 mb-3">
                              <h5 className="mb-0">13 critical-to-close significant deficiency findings complted</h5>
                              <div className="d-flex align-items-center">


                              </div>
                            </div>
                            <p className="text-white font-weight-light pr-lg-2 pr-xl-5">The total number of sessions within the date range. It is the period time a user is actively engaged with your page for updates</p>
                          </div>
                        </div>
                      </div>
                      <div className="col-md-4 item">
                        <div className="d-flex flex-column flex-xl-row mt-5 mt-md-0">
                          <div className="icon icon-c text-white mr-3">
                            <i className="ti-shopping-cart-full icon-lg ml-3"></i>
                          </div>
                          <div className="content text-white">
                            <div className="d-flex flex-wrap align-items-center mb-2 mt-3 mt-xl-1">
                              <h3 className="font-weight-light mr-2 mb-1">Cross Company Access</h3>
                              <h3 className="mb-0">1356</h3>
                            </div>
                            <div className="col-8 col-md-7 d-flex border-bottom border-info align-items-center justify-content-between px-0 pb-2 mb-3">
                              <h5 className="mb-0">325 infrastructure system access</h5>
                              <div className="d-flex align-items-center">


                              </div>
                            </div>
                            <p className="text-white font-weight-light pr-lg-2 pr-xl-5">Completed MVP1 by reinstated access for 700 offshore contractors and Cognizant is ready to return to the Clean Roome
                            </p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="carousel-item">
                    <div className="row">
                      <div className="col-md-4 item">
                        <div className="d-flex flex-column flex-xl-row mt-4 mt-md-0">
                          <div className="icon icon-a text-white mr-3">
                            <i className="ti-cup icon-lg ml-3"></i>
                          </div>
                          <div className="content text-white">
                            <div className="d-flex flex-wrap align-items-center mb-2 mt-3 mt-xl-1">
                              <h3 className="font-weight-light mr-2 mb-1">Privileged Access</h3>
                              <h3 className="mb-0"></h3>
                            </div>
                            <div className="col-8 col-md-7 d-flex border-bottom border-info align-items-center justify-content-between px-0 pb-2 mb-3">
                              <h5 className="mb-0">+238</h5>
                              <div className="d-flex align-items-center">

                              </div>
                            </div>
                            <p className="text-white font-weight-light pr-lg-2 pr-xl-5">The total number of sessions within the date range. It is the period time a user is actively engaged with your website, page or app, etc</p>
                          </div>
                        </div>
                      </div>
                      <div className="col-md-4 item">
                        <div className="d-flex flex-column flex-xl-row mt-5 mt-md-0">
                          <div className="icon icon-b text-white mr-3">
                            <i className="ti-bar-chart icon-lg ml-3"></i>
                          </div>
                          <div className="content text-white">
                            <div className="d-flex flex-wrap align-items-center mb-2 mt-3 mt-xl-1">
                              <h3 className="font-weight-light mr-2 mb-1">Active Directory </h3>
                              <h3 className="mb-0">Successfully deployed AD sync event</h3>
                            </div>
                            <div className="col-8 col-md-7 d-flex border-bottom border-info align-items-center justify-content-between px-0 pb-2 mb-3">
                              <h5 className="mb-0">Launch AD Application migration</h5>
                              <div className="d-flex align-items-center">

                              </div>
                            </div>
                            <p className="text-white font-weight-light pr-lg-2 pr-xl-5">The total number of sessions within the date range. It is the period time a user is actively engaged with your website, page or app, etc</p>
                          </div>
                        </div>
                      </div>
                      <div className="col-md-4 item">
                        <div className="d-flex flex-column flex-xl-row mt-5 mt-md-0">
                          <div className="icon icon-c text-white mr-3">
                            <i className="ti-shopping-cart-full icon-lg ml-3"></i>
                          </div>
                          <div className="content text-white">
                            <div className="d-flex flex-wrap align-items-center mb-2 mt-3 mt-xl-1">
                              <h3 className="font-weight-light mr-2 mb-1">Client Authentication & Authorization</h3>
                              <h3 className="mb-0">6358</h3>
                            </div>
                            <div className="col-8 col-md-7 d-flex border-bottom border-info align-items-center justify-content-between px-0 pb-2 mb-3">
                              <h5 className="mb-0">+9082</h5>
                              <div className="d-flex align-items-center">
                                <i className="ti-angle-down mr-2"></i>
                                <h5 className="mb-0">35.54%</h5>
                              </div>
                            </div>
                            <p className="text-white font-weight-light pr-lg-2 pr-xl-5">The total number of sessions within the date range. It is the period time a user is actively engaged with your website, page or app, etc</p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </Slider>
              </div>
            </div>
          </div>
        </div>

        <div className="row">
          <div className="col-md-6 grid-margin stretch-card">
            <div className="card">
              <div className="card-body">
                <p className="card-title">Requests By LoB</p>
                <p className="text-muted font-weight-light">The total number of requests within the date range. It is to see open requests by LoB and the trend of blocked requests.</p>
                <div className="d-flex flex-wrap mb-5">
                  <div className="mr-5 mt-3">
                    <p className="text-muted">LoB Requests</p>
                    <h3>40</h3>
                  </div>
                  <div className="mr-5 mt-3">
                    <p className="text-muted">Open</p>
                    <h3>20</h3>
                  </div>
                  <div className="mr-5 mt-3">
                    <p className="text-muted">Blocked</p>
                    <h3>5</h3>
                  </div>
                  <div className="mt-3">
                    <p className="text-muted">Commpleted</p>
                    <h3>10</h3>
                  </div>
                </div>
                <Line data={this.ordersChartData} options={this.ordersChartOptions} />
              </div>
            </div>
          </div>
          <div className="col-md-6 grid-margin stretch-card">
            <div className="card">
              <div className="card-body">
                <p className="card-title">Intake Report</p>
                <p className="text-muted font-weight-light">The total number of sessions within the date range. It is the period time a user is actively engaged with your website, page or app, etc</p>
                <div id="sales-legend" className="sales-report-legend mt-4 mb-2">
                  <ul className="1-legend">
                    <li>
                      <span></span>
                      Unstructured Requests
                    </li>
                    <li>
                      <span></span>
                      Catalog Service Requests
                    </li>
                  </ul>
                </div>
                <Bar data={this.SalesReportData} options={this.SalesReportOptions} />
              </div>
              <div className="card border-right-0 border-left-0 border-bottom-0 border-top shadow-none">
                <div className="d-flex justify-content-center justify-content-md-end">
                  <div className="dropdown flex-md-grow-1 flex-xl-grow-0">
                    <Dropdown>
                      <Dropdown.Toggle variant="btn btn-lg btn-outline-dark text-body border border-top-0 border-bottom-0 rounded-0" id="dropdownMenuSizeButton1">
                          Today
                      </Dropdown.Toggle>
                      <Dropdown.Menu>
                        <Dropdown.Item>January - March</Dropdown.Item>
                        <Dropdown.Item>March - June</Dropdown.Item>
                        <Dropdown.Item>June - August</Dropdown.Item>
                        <Dropdown.Item>August - November</Dropdown.Item>
                      </Dropdown.Menu>
                    </Dropdown>
                  </div>
                  <button className="btn btn-lg btn-outline-dark text-primary rounded-0 border-0 d-none d-md-block" type="button"> View all </button>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div className="row">
          <div className="col-md-12 grid-margin stretch-card">
            <div className="card">
              <div className="card-body">
                <p className="card-title mb-0">IA3 Service List</p>
                <div className="table-responsive">
                  <table className="table table-striped table-borderless">
                    <thead>
                      <tr>
                        <th>Services</th>
                        <th>SLA</th>
                        <th>Owner</th>
                        <th>Status</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>2Factor - Authentication</td>
                        <td className="font-weight-bold">1 Day</td>
                        <td>Dan N</td>
                        <td className="font-weight-medium text-success">Completed</td>
                      </tr>
                      <tr>
                        <td>Access Administration - IAM</td>
                        <td className="font-weight-bold">1 Day</td>
                        <td>Clark D</td>
                        <td className="font-weight-medium text-success">Completed</td>
                      </tr>
                      <tr>
                        <td>Access Administration - Distributed Systems</td>
                        <td className="font-weight-bold">1 day</td>
                        <td>Clark D</td>
                        <td className="font-weight-medium text-warning">Pending</td>
                      </tr>
                      <tr>
                        <td>Access Adminstration - Physical Systems</td>
                        <td className="font-weight-bold">3 business days</td>
                        <td>Eric D</td>
                        <td className="font-weight-medium text-warning">Pending</td>
                      </tr>
                      <tr>
                        <td>Active Directory Infrastructure</td>
                        <td className="font-weight-bold">1 Day</td>
                        <td>Amy A</td>
                        <td className="font-weight-medium text-danger">Open</td>
                      </tr>
                      <tr>
                        <td>CICS Change Transaction Request</td>
                        <td className="font-weight-bold">1 Day</td>
                        <td>Amy A</td>
                        <td className="font-weight-medium text-warning">Pending</td>
                      </tr>
                      <tr>
                        <td>CICS File Request</td>
                        <td className="font-weight-bold">2 business days</td>
                        <td>Dan K</td>
                        <td className="font-weight-medium text-success">Completed</td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-12 grid-margin stretch-card">
            <div className="card border-0 position-relative detailed-reports-carousel">
              <div className="card-body">
                <p className="card-title">Detailed IA3 Reports</p>
                <Slider {...detailedReports}>
                  <div className="carousel-item">
                    <div className="row">
                      <div className="col-md-12 col-xl-3 d-flex flex-column justify-content-center">
                        <div className="ml-xl-4">
                          <h1>500</h1>
                          <h3 className="font-weight-light mb-xl-4">Total Requests - 2020</h3>
                          <p className="text-muted mb-2 mb-xl-0">The total number of sessions within the date range. It is the period time a user is actively engaged with your website, page or app, etc</p>
                        </div>
                      </div>
                      <div className="col-md-12 col-xl-9">
                        <div className="row">
                          <div className="col-md-6">
                            <div className="table-responsive mb-3 mb-md-0">
                              <table className="table table-borderless report-table">
                                <tbody>
                                  <tr>
                                    <td className="text-muted">Client Access</td>
                                    <td className="w-100 px-0">
                                      <ProgressBar variant="success" className="progress-md mx-4" now={70}/>
                                    </td>
                                    <td><h5 className="font-weight-bold mb-0">713</h5></td>
                                  </tr>
                                  <tr>
                                    <td className="text-muted">Internal Authentication & Access</td>
                                    <td className="w-100 px-0">
                                      <ProgressBar variant="warning" className="progress-md mx-4" now={30}/>
                                    </td>
                                    <td><h5 className="font-weight-bold mb-0">583</h5></td>
                                  </tr>
                                  <tr>
                                    <td className="text-muted">Access Administration</td>
                                    <td className="w-100 px-0">
                                      <ProgressBar variant="danger" className="progress-md mx-4" now={90}/>
                                    </td>
                                    <td><h5 className="font-weight-bold mb-0">924</h5></td>
                                  </tr>
                                  <tr>
                                    <td className="text-muted">Attestations</td>
                                    <td className="w-100 px-0">
                                      <ProgressBar variant="info" className="progress-md mx-4" now={60}/>
                                    </td>
                                    <td><h5 className="font-weight-bold mb-0">664</h5></td>
                                  </tr>
                                  <tr>
                                    <td className="text-muted">Priviledged Access</td>
                                    <td className="w-100 px-0">
                                      <ProgressBar variant="success" className="progress-md mx-4" now={40}/>
                                    </td>
                                    <td><h5 className="font-weight-bold mb-0">560</h5></td>
                                  </tr>
                                  <tr>
                                  <td className="text-muted">SOX</td>
                                  <td className="w-100 px-0">
                                    <ProgressBar variant="danger" className="progress-md mx-4" now={68}/>
                                  </td>
                                  <td><h5 className="font-weight-bold mb-0">793</h5></td>
                                </tr>
                                </tbody>
                              </table>
                            </div>
                          </div>
                          <div className="col-md-6 mt-3">
                            <Doughnut data={this.northAmericaData} options={this.northAmericaOptions} />
                            <div className="detailed-reports-chart-legend north-america-legend">
                              <div className="item d-flex justify-content-between mx-4 mx-xl-5 mt-3">
                                <div className="d-flex align-items-center">
                                  <div className="mr-3 bullet">
                                  </div>
                                  <p className="mb-0">Retail</p>
                                </div>
                                <p className="mb-0">88</p>
                              </div>
                              <div className="item d-flex justify-content-between mx-4 mx-xl-5 mt-3">
                                <div className="d-flex align-items-center">
                                  <div className="mr-3 bullet">
                                  </div>
                                  <p className="mb-0">Commercial</p>
                                </div>
                                <p className="mb-0">93</p>
                              </div>
                              <div className="item d-flex justify-content-between mx-4 mx-xl-5 mt-3">
                                <div className="d-flex align-items-center">
                                  <div className="mr-3 bullet">
                                  </div>
                                  <p className="mb-0">Insurance</p>
                                </div>
                                <p className="mb-0">36</p>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="carousel-item">
                    <div className="row">
                      <div className="col-md-12 col-xl-3 d-flex flex-column justify-content-center">
                        <div className="ml-xl-4">
                          <h1>321</h1>
                          <h3 className="font-weight-light mb-xl-4">Client IA3</h3>
                          <p className="text-muted mb-2 mb-xl-0">It is the period time a user is actively engaged with your website, page or app, etc. The total number of sessions within the date range. </p>
                        </div>
                      </div>
                      <div className="col-md-12 col-xl-9">
                        <div className="row">
                          <div className="col-md-6">
                            <div className="table-responsive mb-3 mb-md-0">
                              <table className="table table-borderless report-table">
                                <tbody>
                                  <tr>
                                    <td className="text-muted">Commercial</td>
                                    <td className="w-100 px-0">
                                      <ProgressBar variant="success" className="progress-md mx-4" now={70}/>
                                    </td>
                                    <td><h5 className="font-weight-bold mb-0">613</h5></td>
                                  </tr>
                                  <tr>
                                    <td className="text-muted">Coporate</td>
                                    <td className="w-100 px-0">
                                      <ProgressBar variant="warning" className="progress-md mx-4" now={30}/>
                                    </td>
                                    <td><h5 className="font-weight-bold mb-0">483</h5></td>
                                  </tr>
                                  <tr>
                                    <td className="text-muted">Digital</td>
                                    <td className="w-100 px-0">
                                      <ProgressBar variant="danger" className="progress-md mx-4" now={90}/>
                                    </td>
                                    <td><h5 className="font-weight-bold mb-0">824</h5></td>
                                  </tr>
                                  <tr>
                                    <td className="text-muted">Insurance</td>
                                    <td className="w-100 px-0">
                                      <ProgressBar variant="info" className="progress-md mx-4" now={60}/>
                                    </td>
                                    <td><h5 className="font-weight-bold mb-0">564</h5></td>
                                  </tr>
                                  <tr>
                                    <td className="text-muted">Retail</td>
                                    <td className="w-100 px-0">
                                      <ProgressBar variant="success" className="progress-md mx-4" now={40}/>
                                    </td>
                                    <td><h5 className="font-weight-bold mb-0">460</h5></td>
                                  </tr>
                                  <tr>
                                    <td className="text-muted">Cyber</td>
                                    <td className="w-100 px-0">
                                      <ProgressBar variant="danger" className="progress-md mx-4" now={68}/>
                                    </td>
                                    <td><h5 className="font-weight-bold mb-0">693</h5></td>
                                  </tr>
                                </tbody>
                              </table>
                            </div>
                          </div>
                          <div className="col-md-6 mt-3">
                            <Doughnut data={this.southAmericaData} options={this.southAmericaOptions} />
                            <div className="detailed-reports-chart-legend north-america-legend">
                              <div className="item d-flex justify-content-between mx-4 mx-xl-5 mt-3">
                                <div className="d-flex align-items-center">
                                  <div className="mr-3 bullet">
                                  </div>
                                  <p className="mb-0">Commerical</p>
                                </div>
                                <p className="mb-0">43</p>
                              </div>
                              <div className="item d-flex justify-content-between mx-4 mx-xl-5 mt-3">
                                <div className="d-flex align-items-center">
                                  <div className="mr-3 bullet">
                                  </div>
                                  <p className="mb-0">Retail</p>
                                </div>
                                <p className="mb-0">54</p>
                              </div>
                              <div className="item d-flex justify-content-between mx-4 mx-xl-5 mt-3">
                                <div className="d-flex align-items-center">
                                  <div className="mr-3 bullet">
                                  </div>
                                  <p className="mb-0">Insurance</p>
                                </div>
                                <p className="mb-0">36</p>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </Slider>
              </div>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-md-4 stretch-card grid-margin grid-margin-md-0">
            <div className="card">
              <div className="card-body">
                <p className="card-title mb-0">IA3 Squads</p>
                <div className="table-responsive">
                  <table className="table table-borderless">
                    <thead>
                      <tr>
                        <th className="pl-0 border-bottom">Names</th>
                        <th className="border-bottom">LoBAssignments</th>
                        <th className="border-bottom">Contact</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td className="text-muted pl-0">Dan</td>
                        <td><p className="mb-0"><span className="font-weight-bold mr-2">Commercial</span></p></td>
                        <td className="text-muted">Chat Now</td>
                      </tr>
                      <tr>
                        <td className="text-muted pl-0">Barry</td>
                        <td><p className="mb-0"><span className="font-weight-bold mr-2">Retail</span></p></td>
                        <td className="text-muted">Chat Now</td>
                      </tr>
                      <tr>
                        <td className="text-muted pl-0">Dimitri</td>
                        <td><p className="mb-0"><span className="font-weight-bold mr-2">Insurance</span></p></td>
                        <td className="text-muted">Chat Now</td>
                      </tr>
                      <tr>
                        <td className="text-muted pl-0">Ramya</td>
                        <td><p className="mb-0"><span className="font-weight-bold mr-2">Corporate</span></p></td>
                        <td className="text-muted">Chat Now</td>
                      </tr>
                      <tr>
                        <td className="text-muted pl-0">Montana</td>
                        <td><p className="mb-0"><span className="font-weight-bold mr-2">Digital</span></p></td>
                        <td className="text-muted">Chat Now</td>
                      </tr>
                      <tr>
                        <td className="text-muted pl-0">Jack</td>
                        <td><p className="mb-0"><span className="font-weight-bold mr-2">Cyber</span></p></td>
                        <td className="text-muted">Chat Now</td>
                      </tr>
                      <tr>
                        <td className="text-muted pl-0 pb-0">Guru</td>
                        <td className="pb-0"><p className="mb-0"><span className="font-weight-bold mr-2">All Others</span></p></td>
                        <td className="text-muted pb-0">Chat Now</td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
          <div className="col-md-4 stretch-card">
            <div className="row">
              <div className="col-md-12 grid-margin stretch-card">
                <div className="card">
                  <div className="card-body">
                    <p className="card-title">Charts</p>
                    <div className="charts-data">
                      <div className="mt-3">
                        <p className="text-muted mb-0">Intake Requests</p>
                        <div className="d-flex justify-content-between align-items-center">
                          <div className="w-100">
                            <ProgressBar variant="success" className="progress-md mx-4" now={95}/>
                          </div>
                          <p className="text-muted mb-0">5k</p>
                        </div>
                      </div>
                      <div className="mt-3">
                        <p className="text-muted mb-0">Users</p>
                        <div className="d-flex justify-content-between align-items-center">
                          <div className="w-100">
                            <ProgressBar variant="success" className="progress-md mx-4" now={40}/>
                          </div>
                          <p className="text-muted mb-0">3k</p>
                        </div>
                      </div>
                      <div className="mt-3">
                        <p className="text-muted mb-0">IA3 Online Trainings</p>
                        <div className="d-flex justify-content-between align-items-center">
                          <div className="w-100">
                            <ProgressBar variant="success" className="progress-md mx-4" now={55}/>
                          </div>
                          <p className="text-muted mb-0">992</p>
                        </div>
                      </div>
                      <div className="mt-3">
                        <p className="text-muted mb-0">Visitors</p>
                        <div className="d-flex justify-content-between align-items-center">
                          <div className="w-100">
                            <ProgressBar variant="success" className="progress-md mx-4" now={30}/>
                          </div>
                          <p className="text-muted mb-0">687</p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-md-12 stretch-card grid-margin grid-margin-md-0">
                <div className="card data-icon-card-primary">
                  <div className="card-body">
                    <p className="card-title text-white">Number of Meetings</p>
                    <div className="row">
                      <div className="col-8 text-white">
                        <h3>3404</h3>
                        <p className="text-white font-weight-light mb-0">The total number of events within the date range hosted by IA3</p>
                      </div>
                      <div className="col-4 background-icon">
                        <i className="ti-calendar"></i>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="col-md-4 stretch-card">
            <div className="card">
              <div className="card-body">
                <p className="card-title">Notifications</p>
                <ul className="icon-data-list">
                  <li>
                    <p className="text-primary mb-1">Isabella Becker</p>
                    <p className="text-muted">IA3 dashboard have been created</p>
                    <small className="text-muted">9:30 am</small>
                  </li>
                  <li>
                    <p className="text-primary mb-1">Adam Warren</p>
                    <p className="text-muted">You have done a great job #TW11109872</p>
                    <small className="text-muted">10:30 am</small>
                  </li>
                  <li>
                    <p className="text-primary mb-1">Leonard Thornton</p>
                    <p className="text-muted">Training has completed</p>
                    <small className="text-muted">11:30 am</small>
                  </li>
                  <li>
                    <p className="text-primary mb-1">George Morrison</p>
                    <p className="text-muted">Active Directory updates have been made</p>
                    <small className="text-muted">8:50 am</small>
                  </li>
                  <li>
                    <p className="text-primary mb-1">Ryan Cortez</p>
                    <p className="text-muted">Herbs are fun and easy to grow.</p>
                    <small className="text-muted">9:00 am</small>
                  </li>

                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
const ListItem = (props) => {

  return (
      <li className={(props.isCompleted ? 'completed' : null)}>
          <div className="form-check">
              <label htmlFor="" className="form-check-label">
                  <input className="checkbox" type="checkbox"
                      checked={props.isCompleted}
                      onChange={props.changed}
                      /> {props.children} <i className="input-helper"></i>
              </label>
          </div>
          <i className="remove ti-close" onClick={props.remove}></i>
      </li>
  )
};
export default Dashboard;
